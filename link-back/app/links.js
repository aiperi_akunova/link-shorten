const express = require('express');
const {nanoid} = require('nanoid');
const mongoDb = require('../mongoDb');
const ObjectId = require('mongodb').ObjectId;

const router = express.Router();


router.post('/', async (req, res) => {
  if (!req.body.url) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const urlObj = {
    shortUrl: nanoid(7),
    originalUrl: req.body.url,
  };

  try{
    const db = mongoDb.getDb();
    const result = await db.collection('links').insertOne(urlObj);
    const newUrlObj = await db.collection('links').findOne({_id: new ObjectId(result.insertedId)});

    res.send(newUrlObj);
  }catch {
    res.sendStatus(500)
  }


});

module.exports = router;
