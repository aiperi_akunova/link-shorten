const express = require('express');
const cors = require('cors');
const links = require('./app/links');
const mongoDb = require('./mongoDb');
const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/links', links);


app.get('/:shortUrl', async (req, res) => {
  try{
    const db = mongoDb.getDb();
    const urlObj = await db.collection('links').findOne({shortUrl: req.params.shortUrl});

    if(urlObj){
      res.status(301).redirect(urlObj.originalUrl);
    }else {
      res.sendStatus(404)
    }

  }catch {
    res.sendStatus(500);
  }
});




const run = async ()=>{
    await mongoDb.connect();

    app.listen(port, ()=>{
        console.log('Server started on '+port+' port!');
    });

    process.on('exit', async ()=>{
        console.log('exiting');
        await mongoDb.disconnect();
    });
};

run().catch(e=> console.error(e));