import axios from "axios";

export const FETCH_URL_REQUEST = 'FETCH_URL_REQUEST';
export const FETCH_URL_SUCCESS = 'FETCH_URL_SUCCESS';
export const FETCH_URL_FAILURE = 'FETCH_URL_FAILURE';

export const CREATE_URL_REQUEST = 'CREATE_URL_REQUEST';
export const CREATE_URL_SUCCESS = 'CREATE_URL_SUCCESS';
export const CREATE_URL_FAILURE = 'CREATE_URL_FAILURE';

export const fetchUrlRequest = () => ({type: FETCH_URL_REQUEST});
export const fetchUrlSuccess = () => ({type: FETCH_URL_SUCCESS});
export const fetchUrlFailure = () => ({type: FETCH_URL_FAILURE});

export const createUrlRequest = () => ({type: CREATE_URL_REQUEST});
export const createUrlSuccess = (data) => ({type: CREATE_URL_SUCCESS, payload: data});
export const createUrlFailure = () => ({type: CREATE_URL_FAILURE});

export const fetchURL = () => {
    return async dispatch => {
        try {
            dispatch(fetchUrlRequest());
            await axios.get('http://localhost:8000/link');
            dispatch(fetchUrlSuccess());
        } catch (e) {
            dispatch(fetchUrlFailure());
        }
    };
};

export const createURL = url => {
    return async dispatch => {
        try {
            dispatch(createUrlRequest());

      const response = await axios.post('http://localhost:8000/links', url);
            dispatch(createUrlSuccess(response.data));
        } catch (e) {
            dispatch(createUrlFailure());
            throw e;
        }
    };
};