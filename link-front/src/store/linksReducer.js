import {CREATE_URL_SUCCESS, FETCH_URL_FAILURE, FETCH_URL_REQUEST, FETCH_URL_SUCCESS} from "./linksActions";

const initialState = {
    fetchLoading: false,
    url: null,
};


const linksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_URL_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_URL_SUCCESS:
            return {...state, fetchLoading: false};
        case FETCH_URL_FAILURE:
            return {...state, fetchLoading: false};
        case CREATE_URL_SUCCESS:
            return {...state, url: action.payload}
        default:
            return state;
    }
};

export default linksReducer;