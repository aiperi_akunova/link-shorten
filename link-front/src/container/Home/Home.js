import React, {useState} from 'react';
import './Home.css';
import {useDispatch, useSelector} from "react-redux";
import {createURL} from "../../store/linksActions";

const Home = () => {

    const dispatch = useDispatch();
    const urlState = useSelector(state => state.url);

    const [userUrl, setUserUrl] = useState({
        url: "",
    });


    const onInputChange = e => {
        const {name, value} = e.target;

        setUserUrl(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const onShorten = async (obj)=>{
       await  dispatch(createURL(obj));
       setUserUrl({url:""});
    }

    let url;
    if(urlState){
       url = 'http://localhost:8000/'+urlState.shortUrl;
    }


    return (
        <div className='home'>
            <h1>Shorten your link!</h1>
            <input
                type="text"
                name="url"
                value={userUrl.url}
                autoComplete='off'
                onChange={onInputChange}
            />
            <button onClick={()=>onShorten(userUrl)}>Shorten</button>

            {
                urlState && (
                    <div>
                        <p>Your link now looks like this</p>
                        <a href={'http://localhost:8000/'+urlState.shortUrl}>{url}</a>
                    </div>
                )
            }
        </div>
    );
};

export default Home;